package shemshur.test.controller;


import org.springframework.web.bind.annotation.*;
import shemshur.test.validation.ValidateValues;
import shemshur.test.querycheck.QueryExecutor;

import javax.naming.NamingException;

import static shemshur.test.validation.ValidateValues.checkAllParams;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;


@RestController
@RequestMapping("")
public class MessageController {

    @GetMapping
    public String sendGetResponse(){
        return "index";
    }

    @PostMapping("check")
    public Map<String, String> checkQuery(@RequestBody Map<String,String> message){
        HashMap result = new HashMap<String,String>();

        String paramQuery = message.get("query");
        String paramDbName = message.get("db_name");
        String paramExecutionTimes = message.get("execution_times");
        String paramTimeout = message.get("timeout");

        HashMap<String, Object> resultOfValidation = checkAllParams(paramQuery, paramDbName, paramExecutionTimes, paramTimeout);
        if(resultOfValidation.get("error")==null) {

            String query = (String) resultOfValidation.get("paramQuery");
            String dbName = (String) resultOfValidation.get("paramDbName");
            Integer executionTimes = (Integer) resultOfValidation.get("paramExecutionTimes");
            Integer timeout = (Integer) resultOfValidation.get("paramTimeout");

            QueryExecutor executor = new QueryExecutor(query, dbName, executionTimes, timeout);
            try {
                ArrayList<Integer> results = executor.check();
                double avgExecTime = 0;
                if (results!=null) avgExecTime = ValidateValues.getAverageValue(results);

                result.put("avg_time",Double.toString(avgExecTime));
                result.put("paramExecutionTimes",Integer.toString(executionTimes));
            } catch (TimeoutException e) {
                e.printStackTrace();
            } catch (NamingException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else result.putAll(resultOfValidation);

        return result;
    }
}
