package shemshur.test.querycheck;

import org.springframework.beans.factory.annotation.Autowired;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.concurrent.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPool {
    private static ConcurrentHashMap<String, Connection> connectionHash;
    public static ConnectionPool pool;
    private static Lock lock;
    private static HashSet<String> db_names;

    @Autowired
    DataSource dataSource;

    static {
        lock = new ReentrantLock();
        pool = new ConnectionPool();

    }

    private ConnectionPool() {
        connectionHash = new ConcurrentHashMap<String, Connection>();
        db_names = new HashSet<String>();
    }

    private void createNewConnection(String db_name) throws SQLException, NamingException {
        Context envCtx = (Context) (new InitialContext().lookup("java:comp/env"));
        DataSource ds =null;
        try{   ds = (DataSource) envCtx.lookup("jdbc/"+db_name);
    } catch (NamingException e) {
        throw new NamingException("There is no such DB");
    }
        db_names.add(db_name);
        Connection connection = ds.getConnection();
        connection.setAutoCommit(false);
        connectionHash.put(db_name, connection);
    }

    public Connection getConnection(String db_name, int timeoutSec) throws NamingException, SQLException, TimeoutException, InterruptedException, ExecutionException {
        lock.lock();
        Connection connection = null;
        try {
            connection = connectionHash.get(db_name);
            if (connection==null) {
                if(!db_names.contains(db_name)) {
                    //creation of new connection if DB before is never used
                    try {
                        createNewConnection(db_name);
                    } catch (NamingException e) {
                        throw new NamingException("There is no such DB");
                    }catch (SQLException e) {
                        throw new SQLException("SQLException");
                    }
                }
                //trying to get a new connection
                Callable<Connection> connectionGetter = new ConnectionGetter<>(db_name);
                ExecutorService exec = Executors.newFixedThreadPool(1);
                Future<Connection> result = exec.submit(connectionGetter);
                try {
                    connection = result.get(timeoutSec, TimeUnit.SECONDS);
                } catch (TimeoutException e) {
                    result.cancel(true);
                    throw new TimeoutException("TIMEOUT: Can mot get connection more than".concat(Integer.toString(timeoutSec)).concat(" seconds"));
                } catch (InterruptedException e) {
                    result.cancel(true);
                    throw new InterruptedException("INTERRUPTED THREAD: Can mot get connection more than".concat(Integer.toString(timeoutSec)).concat(" seconds"));
                }// catch (ExecutionException e) {
                  //  result.cancel(true);
                   // throw new ExecutionException("FAILED TO EXECUTE STRING: Can mot get connection more than".concat(Integer.toString(timeoutSec)).concat(" seconds"));
                //}
            }
            else {
               connectionHash.remove(db_name);
            }
        } finally {
            lock.unlock();
        }
        return connection;
    }

    public void closeConnection(String db_name, Connection connection) {
        lock.lock();
        try {
            connectionHash.put(db_name, connection);
        } finally {
            lock.unlock();
        }
    }

    private class ConnectionGetter<String> implements Callable<Connection> {
        private String db_name;
        public ConnectionGetter(String db_name) {
            this.db_name = db_name;
        }

        public Connection call()
        {
            Connection subConnection = null;
            try {
                do {
                    subConnection = connectionHash.get(this.db_name);
                    TimeUnit.MILLISECONDS.sleep(100);
                } while(subConnection == null);
            } catch (InterruptedException ignored) {
    //            log.debug("Interrupted, closing");
            }
            finally {
                return subConnection;
            }
        }
    }

}
