package shemshur.test.querycheck;

import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class QueryExecutor {
   private String query;
   private String db_name;
   private int executionTimes;
   private int timeout;
   public ArrayList<Integer> resultTimes;

   public QueryExecutor(String query, String db_name, Integer executionTimes, Integer timeout){
       this.query = query;
       this.db_name = db_name;
       this.executionTimes = (executionTimes!=null ? executionTimes : 1);
       this.timeout = (timeout!=null ? timeout : 100);
       this.resultTimes = null;
   }

   public ArrayList<Integer> check() throws SQLException, InterruptedException, ExecutionException, TimeoutException, NamingException {
       for(int i=0;i<executionTimes;i++) {
           ConnectionPool pool = ConnectionPool.pool;
           Connection connection = null;
           connection = pool.getConnection(db_name,timeout);
           if (connection==null) {
           //  throw new Exception("Couldn't get connection");
           }
           else {
               Statement stmt = null;
               try {
                   connection.createStatement();
                   long start = System.currentTimeMillis();
                   stmt.executeQuery(query);
                   long finish = System.currentTimeMillis();
                   long timeConsumedMillis = finish - start;
                   int resultTime = (int) TimeUnit.MILLISECONDS.toSeconds(timeConsumedMillis);
                   resultTimes.add(resultTime);
                   connection.commit();
               } catch (SQLException e) {
                 //  throw new Exception("There is an error in your query statement");
               } finally {
                   connection.rollback();
                   pool.closeConnection(db_name, connection);
               }
           }
       }
       return resultTimes;
   }
}
